FROM debian 
RUN apt-get update && apt-get -y install apt-transport-https && apt-get -y install default-jdk && mkdir spark && cd spark && apt-get -y install wget && wget www.scala-lang.org/files/archive/scala-2.11.0.deb && apt-get -y install gnupg && dpkg -i scala-2.11.0.deb && apt-get -f install && echo "deb https://dl.bintray.com/sbt/debian /" | tee -a /etc/apt/sources.list && apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 642AC823 && apt-get update && apt-get -y install sbt && mkdir /home/plp/ && wget https://archive.apache.org/dist/spark/spark-2.1.0/spark-2.1.0-bin-hadoop2.7.tgz && tar xvf spark-2.1.0-bin-hadoop2.7.tgz && rm spark-2.1.0-bin-hadoop2.7.tgz && mv spark-2.1.0-bin-hadoop2.7 /usr/local/spark
COPY . /home/plp/
RUN cd /home/plp/ && sbt assembly
ENV PATH=$PATH:/usr/local/spark/bin:/home/plp
ENV SCALA_HOME=/usr/local/scala/bin
