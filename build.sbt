name := "plpproject"

version := "0.1"

scalaVersion := "2.11.0"

libraryDependencies ++= Seq("org.apache.spark" % "spark-core_2.11" % "2.1.0" % "provided",
  "org.apache.spark" % "spark-sql_2.11" % "2.1.0" % "provided",
  "org.apache.spark" % "spark-mllib_2.11" % "2.1.0" % "provided"
)

mainClass in Compile := Some("Main")

