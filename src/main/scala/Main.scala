import java.time.{LocalDate, LocalDateTime}

import scala.collection.mutable.ListBuffer

/**
  * Entry point class
  */
object Main {
  /**
    * Entry point method
    * @param args the params, usage : &lt;cmd&gt; &lt;year begin&gt; &lt;month begin&gt; &lt;day begin&gt; &lt;hour begin&gt; &lt;year end&gt; &lt;month end&gt; &lt;day end&gt; &lt;hour end&gt;
    */
  def main(args: Array[String]) {
    if (args.length != 0 && args.length != 8) {
      println("Usage : args = <year begin> <month begin> <day begin> <hour begin> <year end> <month end> <day end> <hour end>")
      println("or, if you want to use the last hour available just give no argument (will never work since the dataset is \"deprecated since 2016-08-01\" unless you change your local hour)")
      println(args.length)
    }
    else{
      var date_begin = LocalDateTime.now()
      var date_end = date_begin
      if(args.length == 8){
        /*Get parameters*/
        val year_begin = args(0).toInt
        val month_begin = args(1).toInt
        val day_begin = args(2).toInt
        val hour_begin = args(3).toInt
        val year_end = args(4).toInt
        val month_end = args(5).toInt
        val day_end = args(6).toInt
        val hour_end = args(7).toInt
        date_begin = LocalDateTime.of(year_begin, month_begin, day_begin, hour_begin, 0, 0)
        date_end =  LocalDateTime.of(year_end, month_end, day_end, hour_end, 0, 0)
      }


      /*Generate every hour we will have to handle (those not already processed before).
      * Will be useful for parallelization*/
      val list_date = new ListBuffer[(Int, Int, Int, Int)]()
      var date = date_begin
      while(date.isBefore(date_end) || date.equals(date_end)){
        val year = date.getYear
        val month = date.getMonth.getValue
        val day = date.getDayOfMonth
        val hour = date.getHour
        if(!FileManager.existsFile(FileManager.getResultFilenameFromDate(year, month, day, hour))){
          list_date += ((year, month, day, hour))
        }
        else{
          println("Results already saved for "+FileManager.getDumpFilenameFromDate(year, month, day, hour))
        }
        date = date.plusHours(1)
      }

      /*GO !*/
      DataPipeline.run(list_date)

      println("Work done, see analytics at http://localhost:4040")
      scala.io.StdIn.readLine()
    }

  }
}
