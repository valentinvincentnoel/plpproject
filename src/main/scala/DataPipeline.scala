import org.apache.spark.broadcast.Broadcast
import org.apache.spark.rdd.RDD
import org.apache.spark.mllib.rdd.MLPairRDDFunctions.fromPairRDD

import scala.collection.mutable.ListBuffer

/**
  * Our data pipeline
  */
object DataPipeline {
  /**
    * The SparkContext
    */
  val sc = SparkManager.configSpark()

  /**
    * A Broadcast[Map[(domain_code:String, page_tile:String), true:Boolean] reference to the broadcast formatted blacklist
    */
  val black_list = DataPipeline.getBlackList()

  /**
    * Formats and broadcasts the blacklist on each partition
    * @return a Broadcast[Map[(domain_code:String, page_tile:String), true:Boolean] reference to the broadcast formatted blacklist
    */
  def getBlackList(): Broadcast[scala.collection.Map[(String, String), Boolean]] = {
    val blacklist = sc.textFile(FileManager.blacklist_path)//get the text file as a list of line
      .map(line => line.split(" "))//split each line
      .filter(line => line.length > 1)//get ride of empty lines
      .map(list => ((list(0), list(1)), true))//format for the broadcast
    return sc.broadcast(blacklist.collectAsMap)//to keep a read-only blacklist cached on each partition (blacklist is small enough)
  }

  /**
    * (for test only)
    * Gets the dump in local resources
    * @param year int
    * @param month int
    * @param day int
    * @param hour int
    * @return the RDD[String] associate to the uncompressed file (each entry is a line)
    */
  def getLocalDump(year:Int, month:Int, day:Int, hour:Int): RDD[String] = {
    return sc.textFile(FileManager.getDumpLocalPathFromDate(year, month, day, hour))
  }


  /**
    * Gets the dump from the http endpoint
    * Using the hdfs access of wikimedia would be better, but we should be granted the access :
    * https://wikitech.wikimedia.org/wiki/Analytics/Data_access#Host_access_granted
    * @param year int
    * @param month int
    * @param day int
    * @param hour int
    * @return the RDD[String] associate to uncompressed file (each entry is a line)
    */
  def getDumpFromWiki(year:Int, month:Int, day:Int, hour:Int): RDD[String] = {
    val local_path = FileManager.getDumpLocalPathFromDate(year, month, day, hour)
    val url = FileManager.getDumpURLFromDate(year, month, day, hour)
    FileManager.fileDownloader(url, local_path)
    return sc.textFile(local_path)
  }

  /**
    * Formats the dump
    * @param dump the RDD[String] dump (each entry is a line)
    * @return a rdd couple (domain_code, page_title, count_views, total_response_size)
    */
  def formatDump(dump:RDD[String]): RDD[(String, String, Int, String)] = {
    return dump
      .map(line => line.split(" "))//split each line
      .filter(line => line.length > 3)//get ride of empty lines
      .map(list => (list(0), (list(1), list(2).toInt, list(3))))
      .map{ case(domain_code, (page_title, count_views, total_response_size)) => (domain_code, page_title, count_views, total_response_size) }
  }

  /**
    * Removes every entry (domain_code, page_title) which are in the blacklist
    * @param formatted_pageview the RDD[(domain_code:String, page_title:String, count_views:Int, total_response_size:String)]) formatted dump
    * @return a RDD[((domain_code:String, page_title:String), (count_views:Int, total_response_size:String))] filtered formatted dump
    */
  def filterBlackList(formatted_pageview:RDD[(String, String, Int, String)]): RDD[(String, String, Int, String)] = {
    return formatted_pageview
      .filter{ case(domain_code, page_title, count_views, total_response_size) => !black_list.value.contains((domain_code, page_title)) }//blacklist has been broadcast
  }

  /**
    * Gets the top 25 (count_views, page_title, total_response_size) by domain_code
    * @param formatted_pageview the RDD[(domain_code:String, page_title:String, count_views:Int, total_response_size:String)]) filtered formatted dump
    * @return a RDD[(domain_code:String, Array[(count_views:Int, page_title:String, page_title:String)])] top 25 (count_views, page_title, total_response_size) by domain_code
    */
  def getTop25Article(formatted_pageview:RDD[(String, String, Int, String)]): RDD[(String, Array[(Int, String, String)])] = {
    formatted_pageview
      .map{ case(domain_code, page_title, count_views, total_response_size) => (domain_code, (count_views, page_title, total_response_size))}//format to (key, value)
      .topByKey(25)//!shuffle!
      .sortByKey()//The rdd is no longer ordered by sub_domain (contrary to dump at the beginning) but there is only 25* , so it is not that expensive (only 0,6s on test)*/
  }

  /**
    * Converts the array of a top 25 articles for one subdomain to a formatted string for output
    * @param top_pageview_for_one_subdomain Array[(count_views:Int, page_title:String, total_response_size:String)]
    * @return the formatted string
    */
  def formatResultForOneSubDomain(top_pageview_for_one_subdomain:Array[(Int, String, String)]): String = {
    return top_pageview_for_one_subdomain.aggregate("")(
      (acc, value) => {
          {acc+"\n"+value._1+" "+value._2}},
      (acc1, acc2) => {acc1 + acc2})
  }

  /**
    * Converts the RDD to a formatted string for output
    * @param top_pageview RDD[(domain_code:String, Array[(count_views:Int, page_title:String, page_title:String)])]
    * @return a formatted string
    */
  def formatResult(top_pageview:RDD[(String, Array[(Int, String, String)])]): String = {
    return top_pageview.aggregate("")(
      (acc, value) => {
        acc +
          value._1 +
          "\n" +
          formatResultForOneSubDomain(value._2) +
          "\n\n"
      },
      (acc1, acc2) => {acc1 + acc2}
    )
  }

  /**
    * Saves the results
    * @param top25_by_key RDD[(domain_code:String, Array[(count_views:Int, page_title:String, page_title:String)])]
    * @param year int
    * @param month int
    * @param day int
    * @param hour int
    */
  def saveResults(top25_by_key:RDD[(String, Array[(Int, String, String)])], year:Int, month:Int, day:Int, hour:Int): Unit = {
    FileManager.saveFile(DataPipeline.formatResult(top25_by_key), FileManager.getResultFilenameFromDate(year, month, day, hour))
  }

  /**
    * Deletes the dump
    * @param year int
    * @param month int
    * @param day int
    * @param hour int
    */
  def clean(year:Int, month:Int, day:Int, hour:Int): Unit = {
    FileManager.deleteFile(FileManager.getDumpLocalPathFromDate(year, month, day, hour))
  }

  /**
    * Runs a pipeline for one date
    * @param year int
    * @param month int
    * @param day int
    * @param hour int
    */
  def processOneDate(year:Int, month:Int, day:Int, hour:Int): Unit ={
    println("Please wait, downloading the dump for "+FileManager.getStringFromDate(year, month, day, hour)+"...")
    val dump = getDumpFromWiki(year, month, day, hour)
    //val dump = DataPipeline.getLocalDump(year, month, day, hour)
    println("Download finished, now processing for "+FileManager.getStringFromDate(year, month, day, hour)+"...")
    val formatted_pageview = DataPipeline.formatDump(dump)
    val filtered_pageview = DataPipeline.filterBlackList(formatted_pageview)
    val top25_by_key = DataPipeline.getTop25Article(filtered_pageview)
    DataPipeline.saveResults(top25_by_key, year, month, day, hour)
    DataPipeline.clean(year, month, day, hour)
    println("Results saved for "+FileManager.getStringFromDate(year, month, day, hour))
  }

  /**
    * Runs the whole pipeline
    * @param list_date ListBuffer[(year:Int, month:Int, day:Int, hour:Int)]) list of hours to process
    */
  def run(list_date:ListBuffer[(Int, Int, Int, Int)]): Unit = {
    /*sc.parallelize(list_date).foreach{ case(year, month, day, hour) => {
      processOneDate(year, month, day, hour)
    }}*///Cause Over-Parallelization
    for ((year, month, day, hour) <- list_date) processOneDate(year, month, day, hour)
  }
}
