import java.io.{File, PrintWriter}
import java.net.URL
import sys.process._
import scala.language.postfixOps

/**
  * Manages local and remote files
  */
object FileManager {

  /**
    * String, where the resources are
    */
  val resources_path = "src/main/resources/"

  /**
    * String, blacklist path
    */
  val blacklist_path = resources_path+"blacklist_domains_and_pages"

  /**
    * @param year int
    * @param month int
    * @param day int
    * @param hour int
    * @return a string representing a given datetime (wiki dump format)
    */
  def getStringFromDate(year:Int, month:Int, day:Int, hour:Int):String = {
    return year.toString+("%02d".format(month))+("%02d".format(day))+"-"+("%02d".format(hour)+"0000")
  }

  /**
    * @param year int
    * @param month int
    * @param day int
    * @param hour int
    * @return the string filename of the wiki dump of a given date
    */
  def getDumpFilenameFromDate(year:Int, month:Int, day:Int, hour:Int):String = {
    return "pagecounts-"+getStringFromDate(year, month, day, hour)+".gz"
  }

  /**
    * @param year int
    * @param month int
    * @param day int
    * @param hour int
    * @return the string filename of the wiki dump of a given date
    */
  def getResultFilenameFromDate(year:Int, month:Int, day:Int, hour:Int):String = {
    return resources_path+"result-"+getStringFromDate(year, month, day, hour)+".txt"
  }

  /**
    * @param year int
    * @param month int
    * @param day int
    * @param hour int
    * @return the string url of the wiki dump of a given date
    */
  def getDumpURLFromDate(year:Int, month:Int, day:Int, hour:Int):String = {
    val y_str = year.toString
    val m_str = "%02d".format(month)
    return "https://dumps.wikimedia.org/other/pagecounts-all-sites/"+y_str+"/"+y_str+"-"+m_str+"/"+getDumpFilenameFromDate(year, month, day, hour)
  }

  /**
    * @param year int
    * @param month int
    * @param day int
    * @param hour int
    * @return the string local path of the wiki dump of a given date
    */
  def getDumpLocalPathFromDate(year:Int, month:Int, day:Int, hour:Int):String = {
    return resources_path+getDumpFilenameFromDate(year, month, day, hour)
  }

  /**
    * Downloads a file from a given url and saves it locally
    * source : https://stackoverflow.com/questions/24162478/how-to-download-and-save-a-file-from-the-internet-using-scala
    * @param url string url from which it downloads
    * @param filename string filename to which it saves
    */
  def fileDownloader(url: String, filename: String):Unit = {
    new URL(url) #> new File(filename) !!
  }

  /**
    * Save a string to a local file
    * @param content the string content of the file
    * @param filename the string name of the file
    */
  def saveFile(content: String, filename: String):Unit = {
    val pw = new PrintWriter(new File(filename))
    pw.write(content)
    pw.close()
  }

  /**
    * Delete a local file
    * @param filename the string name of the file
    */
  def deleteFile(filename: String): Unit = {
    val fileTemp = new File(filename)
    if (fileTemp.exists) {
      fileTemp.delete()
    }
  }

  /**
    * Check if file exists
    * @param filename the string name of the file
    * @return boolean true if exists
    */
  def existsFile(filename: String): Boolean = {
    val fileTemp = new File(filename)
    return fileTemp.exists()
  }
}
