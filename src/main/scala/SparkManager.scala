import org.apache.spark.{SparkConf, SparkContext}

/**
  * Manages spark
  */
object SparkManager {
  /**
    * Configures spark
    * @return the SparkContext
    */
  def configSpark(): SparkContext = {
    val conf = new SparkConf()
    conf.setMaster("local[*]")//Spark will be run locally with number of threads = number cores on the CPU
    conf.setAppName("View Count")
    val sc = new SparkContext(conf)
    sc.setLogLevel("OFF")//Prevent spark from logging to much informations
    //Logger.getLogger("org.apache.spark.SparkContext").setLevel(Level.WARN)
    //sc.setLogLevel("ALL")//Log all informations (for test)
    return sc
  }
}
