# Wikipedia pageview data pipeline assignment
> Valentin NOEL

## Repo structure
### doc
Contains scaladoc.
Run doc/index.html to see the doc.

### project
Contains project specification, such as the build.properties for sbt.

### src/main/resources
Contains the blacklist and the result files.

### src/main/scala
Contains the source code.

## Prerequisites

You will need docker to run the app :

```shell
sudo apt-get update
sudo apt-get install docker.io
```

## Build

To build the docker image, you should :
- Go to the project root
- And run (can take several minutes) :

```shell
sudo docker build --no-cache -t plp .
```

## Run
To enter the image :

```shell
sudo docker run -p 4040:4040 --name container_plp -t plp
^Ctrl+C
sudo docker exec -it container_plp bash
```
Now you can run
```shell
cd /home/plp
spark-submit target/scala-2.11/plpproject-assembly-0.1.jar <year begin> <month begin> <day begin> <hour begin> <year end> <month end> <day end> <hour end>
```
It will run the pipeline for each hour between the beginning and the end (included)

For example :
```shell
spark-submit target/scala-2.11/plpproject-assembly-0.1.jar 2016 3 1 0 2016 3 1 1
```
Will run the pipeline for each hour between 1st March 2016 0am and 1st March 2016 1am (included)

If you want to see the progress in SparkUI, you could go to http://localhost:4040

For the example above, I have :
![SparkUI at the end of the execution process, for above example](sparkui.png)

The results are in /home/plp/src/main/resources

The outputs are in /src/resources.
Their names are "result-\<year>\<month zero padded (2)>\<day zero padded (2)>-\<hour zero padded (2 on left and 4 on right)>.txt"

The output is formatted as below :


\<domain_code><br /><br />
\<count_views> \<page_title><br />
\<count_views> \<page_title><br />
\<count_views> \<page_title><br />
... (sorted by count_views)<br /><br />
\<domain_code><br /><br />
\<count_views> \<page_title><br />
\<count_views> \<page_title><br />
\<count_views> \<page_title><br />
...<br /><br />
... (sorted by domain_code)

